# The Komputer Store

A dynamic webpage built with HTML, CSS and vanilla JavaScript. It consists of three sections:

1. **The Bank** - a section where the user stores funds and makes bank loans
2. **Work** - a section for increasing earnings and depositing cash into the bank balance
3. **Laptops** - a section that enables the user to select and view information about the merchandise

## How to use

### The Bank

This section shows your current bank balance. This is the amount available for you to buy a laptop. You are free to apply for a loan, but you cannot get a loan that is more than double of your bank balance and you may not have two loans at once.

Once you have a loan, a new button will appear that enables you to repay it.

### Work

This section displays your current salary amount. It lets you know how much money you have earned by "working". This money is not part of your bank balance. You can transfer it by clicking on the "Bank" button. If you have an outstanding loan, 10% of your salary will be deducted and transferred to the outstanding loan amount.

### Laptops

The laptops section has two parts: a laptop selection area and an info section. The selection area provides a menu of available laptops and displays a list of features of the selected laptop. The info section shows other details: an image, name, description as well as the price of the selected laptop.

In addition to the above mentioned details, there is a "Buy Now" button that you can use to buy a laptop. As long as you have enough money, the price will be deducted from your bank balance and you will receive a message confirming the purchase.

You can access the Komputer Store from this link: <https://wesselkonstantinov.gitlab.io/komputer-store-app/>
