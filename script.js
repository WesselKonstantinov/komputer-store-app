// All relevant DOM elements to be used
const balanceValue = document.querySelector("#balance");
const payValue = document.querySelector("#pay");
const loanValue = document.querySelector("#loan");
const loanNameValueGroup = document.querySelector(".tile__item--loan");
const getLoanButton = document.querySelector("#get-loan-button");
const repayLoanButton = document.querySelector("#repay-loan-button");
const bankButton = document.querySelector("#bank-button");
const workButton = document.querySelector("#work-button");
const buyLaptopButton = document.querySelector("#buy-laptop-button");
const laptopSelectMenu = document.querySelector("select");
const laptopFeaturesList = document.querySelector("#laptop-features-list");
const laptopImage = document.querySelector("#laptop-image");
const laptopTitle = document.querySelector("#laptop-title");
const laptopDescription = document.querySelector("#laptop-description");
const laptopPrice = document.querySelector("#laptop-price");

// Initial values
let bankBalance = 0;
let pay = 0;
let loan = 0;
let currentlyDisplayedLaptop;

const API_BASE_URL = "https://noroff-komputer-store-api.herokuapp.com";

// Returns the data of one laptop from all fetched laptops based on a given title
const getSelectedLaptopByTitle = async (laptopTitle) => {
  const response = await fetch(`${API_BASE_URL}/computers`);
  const data = await response.json();
  const selectedLaptop = data.find((laptop) => laptop.title === laptopTitle);
  return selectedLaptop;
};

// Renders the data of the selected laptop in the DOM
const renderLaptopDetails = (laptop) => {
  // First empty the contents of the features list, so that the features of only one laptop get displayed
  laptopFeaturesList.innerHTML = "";
  laptop.specs.forEach((spec) => {
    const listItem = document.createElement("li");
    listItem.textContent = spec;
    laptopFeaturesList.appendChild(listItem);
  });

  laptopImage.src = `${API_BASE_URL}/${laptop.image}`;
  laptopImage.alt = laptop.title;
  laptopTitle.textContent = laptop.title;
  laptopDescription.textContent = laptop.description;
  laptopPrice.textContent = getFormattedCurrency(laptop.price);
};

// Returns a specified sum of money as a currency string
const getFormattedCurrency = (amount) =>
  new Intl.NumberFormat("nl-NL", {
    style: "currency",
    currency: "EUR",
  }).format(amount);

// Prompts the user to enter an amount and adds a new loan if all criteria have been met
const processNewLoan = () => {
  const desiredLoanAmount = prompt("How much would you like to loan?");

  if (desiredLoanAmount === null) {
    return;
  } else if (!desiredLoanAmount || Number.isNaN(Number(desiredLoanAmount))) {
    alert("Please enter a number.");
  } else {
    if (loan) {
      alert("Please repay the last loan before getting a new one.");
    } else if (desiredLoanAmount > bankBalance * 2) {
      alert("You cannot get a loan more than double of your bank balance.");
    } else {
      loan = Number(desiredLoanAmount);
      alert(
        `Your loan (amount: ${getFormattedCurrency(
          desiredLoanAmount
        )}) has been approved.`
      );
      loanValue.textContent = getFormattedCurrency(loan);
      loanNameValueGroup.style.display = "flex";
      repayLoanButton.style.display = "inline-block";
    }
  }
};

// Increases the user's pay balance at a rate of 100
const updatePay = () => {
  pay += 100;
  payValue.textContent = getFormattedCurrency(pay);
};

// Handles repaying the outstanding loan in full
const repayLoan = () => {
  if (loan > pay) {
    alert("You don't have enough money to repay this loan.");
    return;
  }

  pay -= loan;

  // Reset value to 0
  loan -= loan;
  payValue.textContent = getFormattedCurrency(pay);
  loanNameValueGroup.style.display = "none";
  repayLoanButton.style.display = "none";
};

// Transfers money from the pay balance to the bank balance (and loan amount in case of an outstanding loan)
const transferPay = () => {
  if (loan) {
    const oneTenthOfPay = pay * 0.1;
    pay -= oneTenthOfPay;
    loan -= oneTenthOfPay;
    bankBalance += pay;
    pay -= pay;

    if (loan <= 0) {
      // Transfer the remainder after the 10% deduction back to the bank balance and set loan to 0 to avoid negative values
      bankBalance -= loan;
      loan = 0;
      loanNameValueGroup.style.display = "none";
      repayLoanButton.style.display = "none";
    }
  } else {
    bankBalance += pay;
    pay -= pay;
  }

  balanceValue.textContent = getFormattedCurrency(bankBalance);
  payValue.textContent = getFormattedCurrency(pay);
  loanValue.textContent = getFormattedCurrency(loan);
};

// Handles buying a new laptop
const buyLaptop = () => {
  if (bankBalance < currentlyDisplayedLaptop.price) {
    alert("You don't have enough money to buy this laptop.");
    return;
  }

  bankBalance -= currentlyDisplayedLaptop.price;
  balanceValue.textContent = getFormattedCurrency(bankBalance);
  alert("Your laptop has been successfully purchased! Have fun with it!");
};

// Initial display of values and laptop details on page load
balanceValue.textContent = getFormattedCurrency(bankBalance);
payValue.textContent = getFormattedCurrency(pay);
getSelectedLaptopByTitle(laptopSelectMenu.value)
  .then((selectedLaptop) => {
    currentlyDisplayedLaptop = selectedLaptop;
    renderLaptopDetails(selectedLaptop);
  })
  .catch((err) => console.log(err));

getLoanButton.addEventListener("click", processNewLoan);
repayLoanButton.addEventListener("click", repayLoan);
bankButton.addEventListener("click", transferPay);
workButton.addEventListener("click", updatePay);
buyLaptopButton.addEventListener("click", buyLaptop);
laptopSelectMenu.addEventListener("change", (e) => {
  getSelectedLaptopByTitle(e.target.value)
    .then((selectedLaptop) => {
      currentlyDisplayedLaptop = selectedLaptop;
      renderLaptopDetails(selectedLaptop);
    })
    .catch((err) => console.log(err));
});
